#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = '0xE Hacker Club'
SITENAME = '0xE Hacker Club | O primeiro hackerspace de Alagoas'
SITEURL = ''
HOME = 'http://oxehacker.club'
COPYRIGHT = '0xE Hacker Club'
# SITESUBTITLES = ('O primeiro hackerspasce de Alagoas')

PATH = 'content'
SITELOGO = '/theme/images/oxe.svg'
ICONS_URL = '/theme/icons'

TIMEZONE = 'America/Maceio'

DEFAULT_LANG = 'pt-BR'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('0xE Hacker Club', 'http://oxehacker.club/'),
)

# Social widget
SOCIAL = (
    ('Telegram', 'https://t.me/oxehackerclub'),
    ('Twitter', 'https://twitter.com/0xehackerclub'),
    ('Facebook', 'https://www.facebook.com/0xehackerclub/'),
    ('Instagram', 'https://www.instagram.com/0xehackerclub/'),
    ('Contribua', 'https://app.picpay.com/user/edoardo.couto'),
    ('Localização', 'https://maps.app.goo.gl/k6MTb6FGbxaaHt4f6'),
)

DEFAULT_PAGINATION = 8

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'theme'

PLUGINS = ['ical']
