title: Hackerspace
date: 2019-1-1
category: Hackerspace


## 0xE Hacker Club

O 0x3 Hacker Club é um hackerspace localizado na cidade de Maceió, AL, 
Brasil.

## O que é um hackerspace?

Um hackerspace é um espaço comunitário, aberto e colaborativo que 
disponibiliza sua infraestrutura para encontros, eventos e projetos em 
diversas áreas relacionadas a tecnologia e ao que a criatividade permitir, 
fomentando a troca de conhecimento e o compartilhamento de ideias 
(By GaroaHS).

O 0xE Hacker Club é mantido inteiramente com trabalho voluntário e doações 
(em dinheiro e em equipamentos). Você pode ajudar:

- Associe-se;
- Doe :)
